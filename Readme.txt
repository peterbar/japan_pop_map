ADD HERE INFO ON THE PROJECT BEFORE MAKING REPO PUBLIC


Results of code execution are available in the output_maps.zip file. Note that I have done some minor manual tweaks and improvements to the map in PowerPoint format - that's why I needed to save the map in .pptx - to allow for manual edits. 



Bibliography: citations and data sources

Andy Teucher and Kenton Russell. 2018. rmapshaper: Client for 'mapshaper' for 'Geospatial' Operations. R package version 0.4.1. https://CRAN.R-project.org/package=rmapshaper.

GADM Maps and Data. 2018. “GADM Data Version 3.6.” Accessed November 7, 2018. https://gadm.org/.

Hadley Wickham. 2017. tidyverse: Easily Install and Load the 'Tidyverse'. R package version 1.2.1. https://CRAN.R-project.org/package=tidyverse.

Pebesma, E. 2018. Simple Features for R: Standardized Support for Spatial Vector Data. The R Journal. https://journal.r-project.org/archive/2018/RJ-2018-009/.

R Core Team. 2018. R: A language and environment for statistical computing. R Foundation for Statistical Computing, Vienna, Austria. https://www.R-project.org/.

Statistics Japan. 2007. “Current Population Estimates as of October 1, 2007. Table 4: Population by Sex and Sex ratio for Prefectures - Total population, Japanese population, October 1, 2007.”  Accessed November 7, 2018. http://www.stat.go.jp/english/data/jinsui/2007np/index.html.

———. 2017. “Current Population Estimates as of October 1, 2017. Table 4: Population by Sex and Sex ratio for Prefectures - Total population, Japanese population, October 1, 2017.” Accessed November 7, 2018. http://www.stat.go.jp/english/data/jinsui/2017np/index.html.

Tennekes M. 2018. “tmap: Thematic Maps in R.” Journal of Statistical Software, 84(6), 1-39. doi: 10.18637/jss.v084.i06. http://doi.org/10.18637/jss.v084.i06.

Tom Wenseleers and Christophe Vanderaa. 2018. export: Streamlined Export of Graphs and Data Tables. R package version 0.2.2.9000. https://github.com/tomwenseleers/export.



